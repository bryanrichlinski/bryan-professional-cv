const tabs = document.querySelectorAll('[data-tab-target]');

const content = document.querySelectorAll('[data-tab-content]');
/*
tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        const target = document.querySelector(tab.dataset.tabTarget);
        content.forEach(content => {
            if (content.classList.contains('active')) {
                content.classList.remove('active');
            };

        });
        tabs.forEach(tab => {
            if (tab.classList.contains('active')) {
                tab.classList.remove('active');
            };

        }
        );
        target.classList.add('active');
        tab.classList.add('active');

    });

});
*/




tabs.forEach(tab => {


    tab.addEventListener('click', () => {
        
        const target = document.querySelector(tab.dataset.tabTarget);
        content.forEach(content => {
            if (content.classList.contains('active') && target != content) {
                content.classList.remove('visible');
                content.classList.add('hidden');
                
                content.addEventListener('transitionend', () => {
                    content.classList.remove('active');

                    target.classList.add('active');
                    setTimeout(() => {
                        target.classList.add('visible');
                        target.classList.remove('hidden');
                        
                    },
                        20);
                    
                }, { once: true });
            };
        });

        tabs.forEach(tab => {
            if (tab.classList.contains('active')) {
                tab.classList.remove('active');
            };

        });
        
        tab.classList.add('active');
        
    });

});

